export default {
    methods: {
      debounceSearch: _.debounce(function () {
        this.filterItems();
      }, 1000),
    },
  }
  