export default {
    methods: {
        resetFilters() {
            this.filters.search_text = "";
            this.filters.page = 1;
            this.filters.sort = "ASC";
            this.filters.type = "";
            this.filters.per_page = 10;
            this.filters.orderBy = "id";
            this.filterItems();
          }
    }
  }