import Vue from 'vue'
import { router } from '../../main'

import axios from 'axios'

import NProgress from "nprogress";
Vue.use(NProgress);

const state = {
    grades : {},
    gradeDetails : {}
}

const mutations = {
    GET_GRADES(state, payload) {
        state.grades = payload
      },

    GET_GRADE_DETAILS(state, payload) {
      state.gradeDetails = payload
    }
}

const actions = {
    getGrades({ commit }, payload) {
        NProgress.start()
        axios.get('/grades', {
            params: {
              page: payload.page,
              sort: payload.sort,
              search_text: payload.search_text,
              per_page: payload.per_page,
              type: payload.type,
              list: payload.list
            },
            headers: {
              'Authorization': 'Bearer ' + payload.token,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*"
            },
    
          }).then(response => {
            if (response.data.success) {
              NProgress.done()
              commit('GET_GRADES', response.data.data)
            }
          })
          .catch((e) => {
            NProgress.done()
            e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
            Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }    
          })
    },

    getGradeDetails({ commit }, grade) {   
      NProgress.start() 
      axios.get('grades/' + grade.id, {
        headers: {
          'Authorization': 'Bearer '+ grade.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*"
        },

      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          commit('GET_GRADE_DETAILS', response.data.data)
        }
      })
      .catch((e) => {
        NProgress.done()
        e.response.message == "" ? 'There is a problem, please try again' : e.response.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 404) {
          router.push({
            name: "Page404",
          })
        }
        
        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    addGrade({ commit, dispatch }, info) {
      NProgress.start()
      axios.post("/grades", info.user, {
        headers: {
          'Authorization': 'Bearer ' + info.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*",
        }
      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          Vue.swal(response.data.message, "", "success");
          let info = {
            id: response.data.data.id,
            token: info.token
          }
          setTimeout(() => {
            Vue.swal.close()
            dispatch("grades", info)
          }, 2000);
        }
      }).catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    editGrade({ commit }, info) {
      info.user.append("_method", 'patch')
      NProgress.start()
      axios.post("/grades/" + info.id, info.user, {
        headers: {
          'Authorization': 'Bearer ' + info.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*",
        }
      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          Vue.swal(response.data.message, "", "success");
          setTimeout(() => {
            Vue.swal.close()
            router.push('/grades')
          }, 2000);
        }
      }).catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 404) {
          router.push({
            name: "Page404",
          })
        }

        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    deleteGrade({ commit, dispatch }, info) {
      Vue.swal({
        title: "Are you sure?",
        text: "You will not be able to recover this lorem ipsum!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!"
      }).then(result => {
        if (result.value) {  
          NProgress.start()                  
          axios.delete('/grades', {
            params: {
              ids: info.ids,
            },
            headers: {
              'Authorization': 'Bearer ' + info.token,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*",
            }
          }).then(response => {
            if (response.data.success) {
              NProgress.done()
              Vue.swal(response.data.message, "", "success");
              dispatch('getGrades', info)
            }
          }).catch((e) => {
            NProgress.done()
            e.response.data.message == "" ? 'error accured, please try again' : e.response.data.message
            Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }
          })
        } else {
          Vue.swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
        Vue.swal.closeModal();
      });
    },
}

export default {
  state,
  mutations,
  actions
}
