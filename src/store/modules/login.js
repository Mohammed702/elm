import Vue from 'vue'
import axios from 'axios'
import { router } from '../../main.js'

import NProgress from "nprogress";
Vue.use(NProgress);

const state = {
  loginUser: JSON.parse(localStorage.getItem("loginUser"))
}

const mutations = {}

const actions = {
  login({ commit }, user) {
    NProgress.start()
    axios.post('/login', user, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'cache-control': ' no-cache',
        "Access-Control-Allow-Origin": "*",
      }
    }).then(response => {      
      if (response.data.success && response.data.data.token) {
        NProgress.done()
        Vue.swal(response.data.message, "", "success");
        localStorage.setItem('loginUser', JSON.stringify(response.data.data));
        setTimeout(() => router.push({
          name: 'Home',
        }), 2000);
        setTimeout(() => router.go({
          name: 'Home',
        }), 2000);
      } else {
        Vue.swal(response.data.message, "", "error");
      }
    }).catch((e) => {
      NProgress.done()
      e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
      Vue.swal(e.response.data.message, "", "error");
    })
  },

  logout({ commit }, token) {
    NProgress.start()
    axios.post('/logout', token, {
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'cache-control': ' no-cache',
        "Access-Control-Allow-Origin": "*",
      }
    }).then(response => {
      if (response.data.success) {
        NProgress.done()        
        localStorage.clear()
        Vue.swal(response.data.message, "", "success");
        setTimeout(() => router.push({
          name: 'Login'
        }), 2000);
        setTimeout(() => router.go({
          name: 'Login'
        }), 2000);
      } else {
        Vue.swal('There is a problem, please try again');
        localStorage.clear()
        setTimeout(() => router.push({
          name: 'Home'
        }), 1000);
        setTimeout(() => router.go({
          name: 'Home'
        }), 1000);
      }
    }).catch((e) => {
      NProgress.done()
      e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
      Vue.swal(e.response.data.message, "", "error");
      localStorage.clear()
      setTimeout(() => router.push({
        name: 'home'
      }), 2000);
      setTimeout(() => router.go({
        name: 'home'
      }), 2000);
    })
  },
}

export default {
  state,
  mutations,
  actions
}
