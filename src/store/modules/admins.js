const state = {
    admins : {},
    adminDetails : {}
}

const mutations = {
    GET_ADMINS(state, payload) {
        state.admins = payload
      },

    GET_ADMIN_DETAILS(state, payload) {
      state.adminDetails = payload
    }
}

const actions = {
    /*getAdmins({ commit }) {
        axios.get('/admins', {
            headers: {
              'Authorization': 'Bearer ',
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*"
            },
    
          }).then(response => {
            if (response.data.success) {
              commit('GET_ADMINS', response.data.data)
            }
          })
          .catch((e) => {
            e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
          })
      },*/

      getAdminDetails() {
        /*axios.get('/admins', {
          headers: {
            'Authorization': 'Bearer ',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'cache-control': ' no-cache',
            "Access-Control-Allow-Origin": "*"
          },
  
        }).then(response => {
          if (response.data.success) {
            commit('GET_ADMIN_DETAILS', response.data.data)
          }
        })
        .catch((e) => {
          e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        })*/
      },

      addAdmin() {
      },

      editAdmin() {
      },

      deleteAdmin() {
      },
}

export default {
  state,
  mutations,
  actions
}
