import Vue from 'vue'
import { router } from '../../main'

import axios from 'axios'

import NProgress from "nprogress";
Vue.use(NProgress);

const state = {
    teachers : {},
    teacherDetails : {}
}

const mutations = {
    GET_TEACHERS(state, payload) {
        state.teachers = payload
      },

    GET_TEACHER_DETAILS(state, payload) {
      state.teacherDetails = payload
    }
}

const actions = {
  getTeachersNotSupervisor({ commit }, payload) {
    NProgress.start()
    axios.get('/teachers', {
        params: {
          list: payload.list,
          type: payload.type
        },
        headers: {
          'Authorization': 'Bearer ' + payload.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*"
        },

      }).then(response => {
        NProgress.done()
        if (response.data.success) {
          commit('GET_TEACHERS', response.data.data)
        }
      })
      .catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }    
      })
},

    getTeachers({ commit }, payload) {
        NProgress.start()
        axios.get('/teachers', {
            params: {
              page: payload.page,
              sort: payload.sort,
              search_text: payload.search_text,
              per_page: payload.per_page,
              type: payload.type,
              list: payload.list
            },
            headers: {
              'Authorization': 'Bearer ' + payload.token,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*"
            },
    
          }).then(response => {
            NProgress.done()
            if (response.data.success) {
              commit('GET_TEACHERS', response.data.data)
            }
          })
          .catch((e) => {
            NProgress.done()
            e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
            Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }    
          })
    },

    getTeacherDetails({ commit }, teacher) {    
      NProgress.start()
      axios.get('teachers/' + teacher.id, {
        headers: {
          'Authorization': 'Bearer '+ teacher.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*"
        },

      }).then(response => {
        NProgress.done()
        if (response.data.success) {
          commit('GET_TEACHER_DETAILS', response.data.data)
        }
      })
      .catch((e) => {
        NProgress.done()
        e.response.message == "" ? 'There is a problem, please try again' : e.response.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 404) {
          router.push({
            name: "Page404",
          })
        }
        
        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    addTeacher({ commit, dispatch }, info) {
      NProgress.start()
      axios.post("/teachers", info.user, {
        headers: {
          'Authorization': 'Bearer ' + info.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*",
        }
      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          Vue.swal(response.data.message, "", "success");
          let info = {
            id: response.data.data.id,
            token: info.token
          }
          setTimeout(() => {
            Vue.swal.close()
            dispatch("teachers", info)
          }, 2000);
        }
      }).catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }
      })
    },

    editTeacher({ commit }, info) {
      info.user.append("_method", 'patch')
      NProgress.start()
      axios.post("/teachers/" + info.id, info.user, {
        headers: {
          'Authorization': 'Bearer ' + info.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*",
        }
      }).then(response => {
        if (response.data.success) { 
          NProgress.done()         
          Vue.swal(response.data.message, "", "success");
          setTimeout(() => {
            Vue.swal.close()
            router.push('/teachers')
          }, 2000);
        }
      }).catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 404) {
          router.push({
            name: "Page404",
          })
        }

        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    deleteTeacher({ commit, dispatch }, info) {
      Vue.swal({
        title: "Are you sure?",
        text: "You will not be able to recover this lorem ipsum!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!"
      }).then(result => {
        if (result.value) { 
          NProgress.start()                   
          axios.delete('/teachers', {
            params: {
              ids: info.ids,
            },
            headers: {
              'Authorization': 'Bearer ' + info.token,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*",
            }
          }).then(response => {
            if (response.data.success) {
              NProgress.done()
              Vue.swal(response.data.message, "", "success");
              dispatch('getTeachers', info)
            }
          }).catch((e) => {
            NProgress.done()
            e.response.data.message == "" ? 'error accured, please try again' : e.response.data.message
            Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }
          })
        } else {
          Vue.swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
        Vue.swal.closeModal();
      });
    },
}

export default {
  state,
  mutations,
  actions
}
