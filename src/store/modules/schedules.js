import Vue from 'vue'
import { router } from '../../main'

import axios from 'axios'

import NProgress from "nprogress";
Vue.use(NProgress);

const state = {
  schedules : {},
  scheduleDetails : {},

  days : {}
}

const mutations = {
  GET_SCHEDULES(state, payload) {
      state.schedules = payload
    },

  GET_SCHEDULE_DETAILS(state, payload) {
    state.scheduleDetails = payload
  },

  GET_DAYS(state, payload) {
    state.days = payload
  },
}

const actions = {
    getDays({ commit }, payload) {
      NProgress.start()
      axios.get('/days', {
          headers: {
            'Authorization': 'Bearer ' + payload.token,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'cache-control': ' no-cache',
            "Access-Control-Allow-Origin": "*"
          },

        }).then(response => {
          if (response.data.success) {
            NProgress.done()
            commit('GET_DAYS', response.data.data)
          }
        })
        .catch((e) => {
          NProgress.done()
          e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
          Vue.swal(e.response.data.message, "", "error");
          if (e.response.status == 401) {
            router.push({
              name: "Login",
            })
          }    
        })
    },

    getSchedules({ commit }, payload) {
        NProgress.start()
        axios.get('/schedules', {
            params: {
              page: payload.page,
              sort: payload.sort,
              search_text: payload.search_text,
              per_page: payload.per_page,
              type: payload.type,
              list: payload.list
            },
            headers: {
              'Authorization': 'Bearer ' + payload.token,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*"
            },
    
          }).then(response => {
            if (response.data.success) {
              NProgress.done()
              commit('GET_SCHEDULES', response.data.data)
            }
          })
          .catch((e) => {
            NProgress.done()
            e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
            Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }    
          })
    },

    getScheduleDetails({ commit }, schedule) {   
      NProgress.start() 
      axios.get('schedules/' + schedule.id, {
        headers: {
          'Authorization': 'Bearer '+ schedule.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*"
        },

      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          commit('GET_SCHEDULE_DETAILS', response.data.data)
        }
      })
      .catch((e) => {
        NProgress.done()
        e.response.message == "" ? 'There is a problem, please try again' : e.response.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 404) {
          router.push({
            name: "Page404",
          })
        }
        
        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    addSchedule({ commit, dispatch }, info) {
      NProgress.start()
      axios.post("/schedules", info.user, {
        headers: {
          'Authorization': 'Bearer ' + info.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*",
        }
      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          Vue.swal(response.data.message, "", "success");
          let info = {
            id: response.data.data.id,
            token: info.token
          }
          setTimeout(() => {
            Vue.swal.close()
            dispatch("schedules", info)
          }, 2000);
        }
      }).catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    editSchedule({ commit }, info) {
      info.user.append("_method", 'patch')
      NProgress.start()
      axios.post("/schedules/" + info.id, info.user, {
        headers: {
          'Authorization': 'Bearer ' + info.token,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json',
          'cache-control': ' no-cache',
          "Access-Control-Allow-Origin": "*",
        }
      }).then(response => {
        if (response.data.success) {
          NProgress.done()
          Vue.swal(response.data.message, "", "success");
          setTimeout(() => {
            Vue.swal.close()
            router.push('/schedules')
          }, 2000);
        }
      }).catch((e) => {
        NProgress.done()
        e.response.data.message == "" ? 'There is a problem, please try again' : e.response.data.message
        Vue.swal(e.response.data.message, "", "error");
        if (e.response.status == 404) {
          router.push({
            name: "Page404",
          })
        }

        if (e.response.status == 401) {
          router.push({
            name: "Login",
          })
        }
      })
    },

    deleteSchedule({ commit, dispatch }, info) {
      Vue.swal({
        title: "Are you sure?",
        text: "You will not be able to recover this lorem ipsum!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!"
      }).then(result => {
        if (result.value) {  
          NProgress.start()                  
          axios.delete('/schedules', {
            params: {
              ids: info.ids,
            },
            headers: {
              'Authorization': 'Bearer ' + info.token,
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json',
              'cache-control': ' no-cache',
              "Access-Control-Allow-Origin": "*",
            }
          }).then(response => {
            if (response.data.success) {
              NProgress.done()
              Vue.swal(response.data.message, "", "success");
              dispatch('getSchedules', info)
            }
          }).catch((e) => {
            NProgress.done()
            e.response.data.message == "" ? 'error accured, please try again' : e.response.data.message
            Vue.swal(e.response.data.message, "", "error");
            if (e.response.status == 401) {
              router.push({
                name: "Login",
              })
            }
          })
        } else {
          Vue.swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
        Vue.swal.closeModal();
      });
    }

}

export default {
  state,
  mutations,
  actions
}
