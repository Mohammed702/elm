import Vue from 'vue'
import Vuex from 'vuex'

import teachers from './modules/teachers'
import students from './modules/students'
import guardians from './modules/guardians'
import admins from './modules/admins'

import years from './modules/years'
import semesters from './modules/semesters'
import sections from './modules/sections'
import grades from './modules/grades'
import gradeTemplates from './modules/gradeTemplates'
import courseTemplates from './modules/courseTemplates'
import courses from './modules/courses'
import schedules from './modules/schedules'

import login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    teachers,
    students,
    guardians,
    admins,

    years,
    semesters,
    sections,
    grades,
    gradeTemplates,
    courseTemplates,
    courses,
    schedules,
    
    login,
  }
})
