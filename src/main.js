
import 'core-js/es6/promise'
import 'core-js/es6/string'
import 'core-js/es7/array'

// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
window.axios = require('axios')
import BootstrapVue from 'bootstrap-vue'
import NProgress from 'nprogress'

import App from './App'
import { routes } from './routes'
import store from './store/store.js'

import VeeValidate from 'vee-validate';
import VueSweetalert2 from 'vue-sweetalert2';

import lodash from 'lodash'
import '../node_modules/nprogress/nprogress.css'

import axios from 'axios'
Vue.use(axios)
axios.defaults.baseURL = 'http://192.168.0.113:8000/v1'   //Othman
//axios.defaults.baseURL = 'http://192.168.0.101:7770/v1'   //Mohammed
Vue.prototype.$http = window.axios

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css"

Vue.use(VueSweetalert2);

Vue.use(BootstrapVue)
Vue.use(VeeValidate);
Vue.use(VueRouter);

Vue.use(lodash)
Vue.use(NProgress)

export const router = new VueRouter({
  routes,
  mode: 'hash',
})

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
