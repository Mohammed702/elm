// Containers
import DefaultContainer from './containers/DefaultContainer';
import Dashboard from './components/Dashboard'

import Start from './components/Start.vue'

import Teachers from './components/teachers/List.vue'
import Show from './components/teachers/teacher/Show.vue'
import AddEditTeacher from './components/teachers/teacher/AddEdit.vue'

import Students from './components/students/List.vue'
import ShowStudent from './components/students/student/Show.vue'
import AddEditStudent from './components/students/student/AddEdit.vue'

import Guardians from './components/guardians/List.vue'
import ShowGuardian from './components/guardians/guardian/Show.vue'
import AddEditGuardian from './components/guardians/guardian/AddEdit.vue'

import Admins from './components/admins/List.vue'
import ShowAdmin from './components/admins/admin/Show.vue'
import AddEditAdmin from './components/admins/admin/AddEdit.vue'

import Year from './components/years/List.vue'
import ShowYear from './components/years/year/Show.vue'
import AddEditYear from './components/years/year/AddEdit.vue'

import Semester from './components/semesters/List.vue'
import ShowSemester from './components/semesters/semester/Show.vue'
import AddEditSemester from './components/semesters/semester/AddEdit.vue'

import Section from './components/sections/List.vue'
import ShowSection from './components/sections/section/Show.vue'
import AddEditSection from './components/sections/section/AddEdit.vue'

import Grade from './components/grades/List.vue'
import ShowGrade from './components/grades/grade/Show.vue'
import AddEditGrade from './components/grades/grade/AddEdit.vue'

import GradeTemplate from './components/gradeTemplates/List.vue'
import ShowGradeTemplate from './components/gradeTemplates/gradeTemplate/Show.vue'
import AddEditGradeTemplate from './components/gradeTemplates/gradeTemplate/AddEdit.vue'

import CourseTemplate from './components/courseTemplates/List.vue'
import ShowCourseTemplate from './components/courseTemplates/courseTemplate/Show.vue'
import AddEditCourseTemplate from './components/courseTemplates/courseTemplate/AddEdit.vue'

import Course from './components/courses/List.vue'
import ShowCourse from './components/courses/course/Show.vue'
import AddEditCourse from './components/courses/course/AddEdit.vue'

import CourseDetails from './components/courseDetailses/List.vue'
import ShowCourseDetails from './components/courseDetailses/courseDetails/Show.vue'
import AddEditCourseDetails from './components/courseDetailses/courseDetails/AddEdit.vue'

import Schedules from './components/schedules/List.vue'
import ShowSchedule from './components/schedules/schedule/Show.vue'
import AddEditSchedule from './components/schedules/schedule/AddEdit.vue'

import Login from './pages/Login.vue'
import Page404 from './pages/Page404.vue'

export const routes = [{
    path: '/',
    redirect: '/dashboard',
    name: 'Home',
    component: DefaultContainer,
    children: [{
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard,
      },

      {
        path: '/teachers',
        component: Start,
        children: [{
          path: '/',
          name: 'Teachers',
          component: Teachers,
        },
        {
          path: 'add',
          name: 'Add Teacher',
          component: AddEditTeacher,
        },
        {
          path: ':id',
          name: 'Show Teacher',
          component: Show
        },
        {
          path: ':id/edit',
          name: 'Edit Teacher',
          component: AddEditTeacher,
        }]
      },

      {
        path: '/students',
        component: Start,
        children: [{
          path: '/',
          name: 'Students',
          component: Students,
        },
        {
          path: 'add',
          name: 'Add Student',
          component: AddEditStudent,
        },
        {
          path: ':id',
          name: 'Show Student',
          component: ShowStudent,
        },
        {
          path: ':id/edit',
          name: 'Edit Student',
          component: AddEditStudent,
        }]
      },

      {
        path: '/guardians',
        component: Start,
        children: [{
          path: '/',
          name: 'Guardians',
          component: Guardians,
        },
        {
          path: 'add',
          name: 'Add Guardian',
          component: AddEditGuardian,
        },
        {
          path: ':id',
          name: 'Show Guardian',
          component: ShowGuardian,
        },
        {
          path: ':id/edit',
          name: 'Edit Guardian',
          component: AddEditGuardian,
        }]
      },

      {
        path: '/admins',
        component: Start,
        children: [{
          path: '/',
          name: 'admins',
          component: Admins,
        },
        {
          path: '/showAdmin',
          name: 'Show Admin',
          component: ShowAdmin,
        },
        {
          path: '/addEditAdmin',
          name: 'AddEdit Admin',
          component: AddEditAdmin,
        }]
      },

      {
        path: '/grades',
        component: Start,
        children: [{
          path: '/',
          name: 'Grades',
          component: Grade,
        },
        {
          path: 'add',
          name: 'Add Grade',
          component: AddEditGrade,
        },
        {
          path: ':id',
          name: 'Show Grade',
          component: ShowGrade,
        },
        {
          path: ':id/edit',
          name: 'Edit Grade',
          component: AddEditGrade,
        }]
      },

      {
        path: '/sections',
        component: Start,
        children: [{
          path: '/',
          name: 'Sections',
          component: Section,
        },
        {
          path: 'add',
          name: 'Add Section',
          component: AddEditSection,
        },
        {
          path: ':id',
          name: 'Show Section',
          component: ShowSection,
        },
        {
          path: ':id/edit',
          name: 'Edit Section',
          component: AddEditSection,
        }]
      },

      {
        path: '/semesters',
        component: Start,
        children: [{
          path: '/',
          name: 'Semesters',
          component: Semester,
        },
        {
          path: 'add',
          name: 'Add Semester',
          component: AddEditSemester,
        },
        {
          path: ':id',
          name: 'Show Semester',
          component: ShowSemester,
        },
        {
          path: ':id/edit',
          name: 'Edit Semester',
          component: AddEditSemester,
        }]
      },

      {
        path: '/years',
        component: Start,
        children: [{
          path: '/',
          name: 'Years',
          component: Year,
        },
        {
          path: 'add',
          name: 'Add Year',
          component: AddEditYear,
        },
        {
          path: ':id',
          name: 'Show Year',
          component: ShowYear,
        },
        {
          path: ':id/edit',
          name: 'Edit Year',
          component: AddEditYear,
        }]
      },

      {
        path: '/gradeTemplates',
        component: Start,
        children: [{
          path: '/',
          name: 'GradeTemplates',
          component: GradeTemplate,
        },
        {
          path: 'add',
          name: 'Add GradeTemplate',
          component: AddEditGradeTemplate,
        },
        {
          path: ':id',
          name: 'Show GradeTemplate',
          component: ShowGradeTemplate,
        },
        {
          path: ':id/edit',
          name: 'Edit GradeTemplate',
          component: AddEditGradeTemplate,
        }]
      },

      {
        path: '/courseTemplates',
        component: Start,
        children: [{
          path: '/',
          name: 'CourseTemplates',
          component: CourseTemplate,
        },
        {
          path: 'add',
          name: 'Add CourseTemplate',
          component: AddEditCourseTemplate,
        },
        {
          path: ':id',
          name: 'Show CourseTemplate',
          component: ShowCourseTemplate,
        },
        {
          path: ':id/edit',
          name: 'Edit CourseTemplate',
          component: AddEditCourseTemplate,
        }]
      },

      {
        path: '/courses',
        component: Start,
        children: [{
          path: '/',
          name: 'Courses',
          component: Course,
        },
        {
          path: 'add',
          name: 'Add Course',
          component: AddEditCourse,
        },
        {
          path: ':id',
          name: 'Show Course',
          component: ShowCourse,
        },
        {
          path: ':id/edit',
          name: 'Edit Course',
          component: AddEditCourse,
        }]
      },

      {
        path: '/coursesDetails',
        component: Start,
        children: [{
          path: '/',
          name: 'CoursesDetails',
          component: CourseDetails,
        },
        {
          path: 'add',
          name: 'Add CourseDetails',
          component: AddEditCourseDetails,
        },
        {
          path: ':id',
          name: 'Show CourseDetails',
          component: ShowCourseDetails,
        },
        {
          path: ':id/edit',
          name: 'Edit CourseDetails',
          component: AddEditCourseDetails,
        }]
      },

      {
        path: '/schedules',
        component: Start,
        children: [{
          path: '/',
          name: 'Schedules',
          component: Schedules,
        },
        {
          path: 'add',
          name: 'Add Schedule',
          component: AddEditSchedule,
        },
        {
          path: ':id',
          name: 'Show Schedule',
          component: ShowSchedule,
        },
        {
          path: ':id/edit',
          name: 'Edit Schedule',
          component: AddEditSchedule,
        }]
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/page404',
    name: 'Page404',
    component: Page404
  }
]
